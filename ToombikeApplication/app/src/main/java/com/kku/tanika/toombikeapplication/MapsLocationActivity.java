package com.kku.tanika.toombikeapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapsLocationActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,TaskLoadedCallback {

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation ;
    private Marker currentUserLocationMarker ,toomLocation, toomCurrent;
    private  static final  int Request_User_Location_Code = 99;
    private  static final  int REQUEST_CALL = 1;
    private ArrayList listPoint;
    private Button cancelService, newPinLocation,editService,saveEdtData,resetPin;
    private TextView address, oldAddress;
    private LinearLayout view1;
    private String strLat ,strLng,phone,userName,sign,bikeModel,trueLat, trueLng, detail, hisNum, oldLat,oldLng;
    private Double lLat, lLong;
    private Double toomLat,toomLng;
    private LatLng currentToom,latLngToom;
    private EditText inTxt;
    private static final String TAG = "CallService";
    private Polyline polyline;
    private RequestQueue queue;
    private Firebase mRootRef;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_location);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        address = (TextView) findViewById(R.id.newPinLocationEdt);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            checkUserLocationPermission();
        }

        try{
            getCurrentToomLocation();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

    }

    public boolean checkUserLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION )!= PackageManager.PERMISSION_GRANTED){

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)){

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},Request_User_Location_Code);
            }else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},Request_User_Location_Code);
            } return false;


        }else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case Request_User_Location_Code:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        if (googleApiClient == null){
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }
                else {
                    Toast.makeText(this,"Permission Denied...",Toast.LENGTH_LONG).show();
                }
        }
    }

    protected  synchronized  void buildGoogleApiClient(){

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

        lastLocation = location;

        if(currentUserLocationMarker != null){
            currentUserLocationMarker.remove();
        }

        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("คุณอยู่ที่นี่");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        currentUserLocationMarker = mMap.addMarker(markerOptions);
        currentUserLocationMarker.showInfoWindow();

        lLat = location.getLatitude();
        lLong = location.getLongitude();

        //lat.setText(String.valueOf(lLat));
        //lng.setText(String.valueOf(lLong));

        strLat = String.valueOf(lLat);
        strLng = String.valueOf(lLong);

        trueLat = strLat;
        trueLng = strLng;


        if (trueLat != null && trueLng != null){
            Geocoder myLocation = new Geocoder(MapsLocationActivity.this, Locale.getDefault());
            List<Address> myList = null;
            try {
                myList = myLocation.getFromLocation(Double.parseDouble(strLat),Double.parseDouble(strLng), 1);
                Address addressName = (Address) myList.get(0);
                String addressStr = "";
                addressStr += addressName.getAddressLine(0);
                address.setText(addressStr);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            finish();
            startActivity(getIntent());
        }

        latLngToom = new LatLng(16.464286,102.829843);
        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(latLngToom);
        markerOptions1.title("ตุ่มมอไซค์");
        markerOptions1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));

        currentUserLocationMarker = mMap.addMarker(markerOptions);
        toomLocation = mMap.addMarker(markerOptions1);
        toomLocation.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.tlt64x64raz));


        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        //the include method will calculate the min and max bound.
        builder.include(currentUserLocationMarker.getPosition());
        builder.include(toomLocation.getPosition());

        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.40); // offset from edges of the map 10% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);
        polyline = mMap.addPolyline(new PolylineOptions()
                .add(latLng)
                .add(latLngToom)
                .width(8f)
                .color(R.color.red)
        );

        mMap.isTrafficEnabled();

        /*mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(14));*/

        if (googleApiClient != null){

            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this);
        }

        String url = getUrl(markerOptions.getPosition(),markerOptions1.getPosition(),"location");
        new FetchUrl(MapsLocationActivity.this).execute(url,"location");

        /*MarkerOptions markerOptionsOld = new MarkerOptions();
        latLngOld = new LatLng(Double.parseDouble(oldLat),Double.parseDouble(oldLng));
        markerOptionsOld.position(latLngOld);
        markerOptionsOld.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        markerOptionsOld.title("ที่อยู่เดิม");
        oldLocation = mMap.addMarker(markerOptionsOld);
        oldLocation.showInfoWindow();*/

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1100);
        locationRequest.setFastestInterval(1100);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,this);
        }
    }

    private String getUrl(LatLng position, LatLng position1, String location) {

        String strOrigin = "origin=" + position.latitude + "," + position.longitude ;
        String strDest = "destination=" + position1.latitude + "," + position1.longitude;
        String mode = "mode=" + location;
        String param = strOrigin + "&" + strDest + "&" + mode;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + param + "&key=" +getString(R.string.google_maps_key);
        return url;
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onTaskDone(Object... values) {
        if(polyline != null){
            polyline.remove();
            polyline = mMap.addPolyline((PolylineOptions) values[0]);
        }
    }

    private void getCurrentToomLocation() {

        firebaseDatabase = FirebaseDatabase.getInstance();

        String[] admin ={"admin@tung" , "test", "web"} ;

        for(int i = 0 ; i < admin.length ; i ++){

            mRootRef = new Firebase("https://toombike-41fdf.firebaseio.com/currentlocation/"+admin[i]+"/lat");

            Log.d("pleum","https://toombike-41fdf.firebaseio.com/currentlocation/"+admin[i]+"/lng");

            mRootRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                    toomLat = (Double) dataSnapshot.getValue();
                    Log.d("toomLat","ToomLocation lat is " + toomLat);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            mRootRef = new Firebase("https://toombike-41fdf.firebaseio.com/currentlocation/"+admin[i]+"/lng");
            mRootRef.addValueEventListener(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                    toomLng = (Double) dataSnapshot.getValue();
                    Log.d("toomLng","ToomLocation lat is " + toomLng);

                    if (toomLat != null && toomLng != null ){
                        final Handler h = new Handler();
                        final int delay = 3 * 1000;

                        h.postDelayed(new Runnable(){
                            public void run(){
                                if (toomCurrent != null){
                                    toomCurrent.remove();
                                }
                                pinCurrentToomLocation();
                                h.postDelayed(this, delay);
                            }
                        }, delay);
                    }else {
                        currentToom = new LatLng(16.464286,102.829843);
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.position(currentToom);
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.motorcyclegreen));
                        toomCurrent = mMap.addMarker(markerOptions);
                        Log.d("toomLocation","ToomLocation location is default" + toomLat +", " + toomLng );
                    }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }

    }

    private void pinCurrentToomLocation() {

        currentToom = new LatLng(toomLat,toomLng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentToom);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.motorcyclegreen));
        toomCurrent = mMap.addMarker(markerOptions);
        Log.d("toomLocation","ToomLocation location is " + toomLat + toomLng);

    }
}
