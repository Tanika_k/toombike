package com.kku.tanika.toombikeapplication;

public class CurrentLocation {
    private String lat, lng;

    public CurrentLocation(){

    }

    public CurrentLocation(String lat, String lng){
        this.lat = lat;
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }
}
