package com.kku.tanika.toombikeapplication;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}