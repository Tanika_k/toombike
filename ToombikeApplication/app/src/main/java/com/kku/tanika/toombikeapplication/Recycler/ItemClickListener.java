package com.kku.tanika.toombikeapplication.Recycler;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view,int position,boolean isLongClick);


}
